#!/bin/bash

# Autor: Chris Rätsepso, 2016

# Skriptimiskeeled kontrolltöö (bash)

# Skripti põhieesmärgiks on kuvada ekraanile kõikvõimalikud 4-kohalised PIN-koodid (alates 0000 kuni 9999)
# ning kirjutada kõik PIN-koodide kombinatsioonid alates kasutaja poolt sisestatud argumendist(peab olema 4-kohaline arv) 
# faili koodid.txt, mis asub kaustas, mille nimeks on kasutaja poolt antud argument.
# Kui aga vastav kaust ja fail on olemas juba, siis luuakse vastavasse kausta uue nimega fail ning sellesse
# faili kirjutataksekõik PIN-koodide kombinatsioonid alates kasutaja poolt sisestatud argumendist.

# Exit koodid:
# 1 - kui kasutaja sisestas vale arvu argumente
# 2 - kui kasutaja ei sisestanud 4-kohalist numbrit ja/või ei sisestanud numbrit

# Kontrollitakse, kas kasutaja sisestas täpselt ühe argumendi

if [ $# -ne 1 ];
then 
    echo "Kasutamine: $0 PIN"
	      exit 1
fi


# Salvestan kasutaja poolt sisestatud argumendi ja selle pikkuse muutujatesse.

PIN=$1
PINPIKKUS=${#PIN}

# Defineerin muutuja, mille abil saab kontrollida, kas kasutaja sisestas ikka arvu.

ON_NUMBER='^[0-9]+$'

# Kontrollin, kas kasutaja poolt sisestatud argument on number ja 4-kohaline.

if [ $PINPIKKUS -eq 4 ] && [[ $PIN =~ $ON_NUMBER ]]; 
then

# Kuvatakse välja PIN-koodid nii kaua kui jõutakse kasutaja sisestatud neljakohalise arvuni (seejärel tsükkel lõpetab töö).

 for i in $(seq -w 0 9999); do
    echo $i

    if [ $i -eq $PIN ];
    then
        break
    fi
  done

# Kui sellist kausta ei ole loodud, mis vastaks kasutaja poolt sisestatud arvule, siis see luuakse,
# seejärel luuakse sinna ka koodid.txt fail ja siis hakkab tsükkel uuesti PIN-koode ekraanile kuvama ning koodid.txt faili
# kirjutama, alustades kasutaja poolt sisestatud neljakohalisest arvust.

 if [ ! -d $PIN ]; 
 then
     mkdir -p $PIN
     touch $PIN/koodid.txt
 
        for a in $(seq -w $PIN 9999); do
            echo $a
            echo $a >> $PIN/koodid.txt
        done

  else

# Kui kaust ja seal kaustas olev koodid.txt fail juba olemas on, pannakse i võrduma arvuga 0 ning
# algab tsükkel, mille sees kontrollitakse, kas koodid-$i.txt fail on olemas- kui ei ole, siis tsükkel lõpetab töö; kui aga on, 
# siis suurendatakse muutujat i ühe võrra, kuni jõutakse sellise numbrini, mille puhul koodid-$i.txt faili olemas ei ole.

i=0

          while [ -f $PIN/koodid-$i.txt ]; do
            let i++
          done
		  
# Nüüd kui tsükkel on tuvastanud sellise failinime, mida veel olemas ei ole, salvestatakse see muutujasse ning seejärel luuakse see fail

    UUSFAIL=$PIN/koodid-$i.txt
    touch $UUSFAIL

# Algab tsükkel, mis kuvab ekraanile kõik ülejäänud PIN-koodid alustades kasutaja poolt sisestatud 
# neljakohalisest numbrist ning ühtlasi kirjutab selle ka uude faili

    for a in $(seq -w $PIN 9999); do
        echo $a
        echo $a >> $UUSFAIL
    done   
   fi
   
   else 
       echo "Sisesta 4-kohaline number!"
	   exit 2
fi







